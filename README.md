
## SULTAN KARYBAYEV

Welcome to "Who wants to be a millionaire" chatbot game.

During the game you will be given some questions with 4 option you to need to choose. More right answers - more scores.

To launch to game you need to download the project, first run "npm install" and then "npm start"
The project will be working on 5000 port, so you can open 'localhost:5000/' on your browser and start playing.

Good luck!

## WEB VERSION

The web version was located on the domain ec2-3-82-210-71.compute-1.amazonaws.com:5000 on the AWS EC2.
Now it's not working, cause the server automatically shuts down itself and after relaunch it will generate an another domain.
The project includes a screen recording file 'Screenrecording.mov' showing how it was working via web.
