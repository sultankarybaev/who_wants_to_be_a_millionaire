let questions = [
    {   question: 'Who was the first human to fly into space?',
        option1: 'Yuri Gagarin',
        option2: 'Neil Armstrong',
        option3: 'Alan Shepard',
        option4: 'Alexey Leonov',
        rightCount: 1,
        rightAnswer: 'Yuri Gagarin'
    },
    {   question: 'What ocean is the largest one?',
        option1: 'Indian Ocean',
        option2: 'Arctic Ocean',
        option3: 'Pacific Ocean',
        option4: 'Atlantic Ocean',
        rightCount: 3,
        rightAnswer: 'Pacific Ocean'
    },
    {   question: 'For what movie did Leonardo DiCaprio win the Oscar?',
        option1: 'Titanic',
        option2: 'The Revenant',
        option3: 'The Shutter Island',
        option4: 'Inception',
        rightCount: 2,
        rightAnswer: 'The Revenant'
    },
    {   question: 'What is the second largest planet in the Solar System?',
        option1: 'Uranus',
        option2: 'Saturn',
        option3: 'Neptune',
        option4: 'Jupiter',
        rightCount: 2,
        rightAnswer: 'Saturn'
    },
    {   question: 'What formula do you use to calculate the area of a circle?',
        option1: '2 * Pi * R',
        option2: 'Pi + 2 * R',
        option3: 'Pi ^ 2 * R',
        option4: 'Pi * R ^ 2',
        rightCount: 4,
        rightAnswer: 'Pi * R ^ 2'
    },
    {   question: 'What is the capital city of Kazakhstan?',
        option1: 'Almaty',
        option2: 'Nur-Sultan',
        option3: 'Taraz',
        option4: 'Shymkent',
        rightCount: 2,
        rightAnswer: 'Nur-Sultan'
    },
    {   question: 'What was the highest-grossing movie of all time?',
        option1: 'Avengers: Endgame',
        option2: 'Titanic',
        option3: 'Furious 7',
        option4: 'Avatar',
        rightCount: 1,
        rightAnswer: 'Avengers: Endgame'
    },
    {   question: 'What country has become a champion of the World Cup the most times?',
        option1: 'Italy',
        option2: 'Germany',
        option3: 'Argentina',
        option4: 'Brazil',
        rightCount: 4,
        rightAnswer: 'Brazil'
    },
    {   question: 'What is the biggest island on Earth?',
        option1: 'Madagascar',
        option2: 'Borneo',
        option3: 'Greenland',
        option4: 'New Guinea',
        rightCount: 3,
        rightAnswer: 'Greenland'
    },
    //--
    {   question: 'Which sea will you not sink in?',
        option1: 'Black Sea',
        option2: 'Mediterranean Sea',
        option3: 'Dead Sea',
        option4: 'Red sea',
        rightCount: 3,
        rightAnswer: 'Dead Sea'
    },
    {   question: 'Who was not a host of the Top Gear show?',
        option1: 'Jeremy Clarkson',
        option2: 'Graham Norton',
        option3: 'Richard Hammond',
        option4: 'James May',
        rightCount: 2,
        rightAnswer: 'Graham Norton'
    },
    {   question: 'How many letters are in the Russian alphabet?',
        option1: '33',
        option2: '29',
        option3: '42',
        option4: '16',
        rightCount: 1,
        rightAnswer: '33'
    },
    {   question: 'What company owns YouTube?',
        option1: 'Amazon',
        option2: 'Microsoft',
        option3: 'NBC',
        option4: 'Google',
        rightCount: 4,
        rightAnswer: 'Google'
    },
    {   question: 'Which of these seas is actually a lake?',
        option1: 'White Sea',
        option2: 'Caspian Sea',
        option3: 'South China Sea',
        option4: 'Sea of Okhotsk',
        rightCount: 2,
        rightAnswer: 'Caspian Sea'
    },
    {   question: 'What was the first car produced by Tesla?',
        option1: 'Tesla Roadster',
        option2: 'Tesla Model X',
        option3: 'Tesla Model S',
        option4: 'Tesla Model 3',
        rightCount: 1,
        rightAnswer: 'Tesla Roadster'
    },
    {   question: 'How many people have been on the Moon?',
        option1: '2',
        option2: '8',
        option3: '12',
        option4: '15',
        rightCount: 3,
        rightAnswer: '12'
    },
    {   question: 'What movie has Christopher Nolan not directed?',
        option1: 'Prestige',
        option2: 'The Dark Knight',
        option3: 'Dunkirk',
        option4: 'Gravity',
        rightCount: 4,
        rightAnswer: 'Gravity'
    },
    {   question: 'What number is Carbon in the Periodic Table?',
        option1: '3',
        option2: '4',
        option3: '5',
        option4: '6',
        rightCount: 4,
        rightAnswer: '6'
    },
    //
    {   question: 'What TV Show was named by Guinness World Records as the highest-rated TV series of all time?',
        option1: 'Breaking Bad',
        option2: 'The Game of Thrones',
        option3: 'Sherlock',
        option4: 'The Office',
        rightCount: 1,
        rightAnswer: 'Breaking Bad'
    },
    {   question: 'What was the name of the first artificial satellite?',
        option1: 'A1',
        option2: 'USSR forward',
        option3: 'Sputnik',
        option4: 'Voyager',
        rightCount: 3,
        rightAnswer: 'Sputnik'
    },
    {   question: 'What year did World War 2 start in?',
        option1: '1940',
        option2: '1941',
        option3: '1939',
        option4: '1943',
        rightCount: 3,
        rightAnswer: '1939'
    },
    {   question: 'What game was the best-selling video game of all time?',
        option1: 'Minecraft',
        option2: 'PUBG',
        option3: 'GTA V',
        option4: 'Tetris',
        rightCount: 1,
        rightAnswer: 'Minecraft'
    },
    {   question: 'What is the largest country that has no access to the sea?',
        option1: 'Pakistan',
        option2: 'Sudan',
        option3: 'Argentina',
        option4: 'Kazakhstan',
        rightCount: 4,
        rightAnswer: 'Kazakhstan'
    },
];

module.exports = class OverAndUnder{

    startTheGame() {
        this.moveCount = 0;
        this.winsCount = 0;
        this.gameStart = true;
    }
    makeAQuestion(){
        let question = questions[this.moveCount];
        this.sReturn.push(question.question);
        this.sReturn.push('1. ' + question.option1);
        this.sReturn.push('2. ' + question.option2);
        this.sReturn.push('3. ' + question.option3);
        this.sReturn.push('4. ' + question.option4);
    }

    makeRightAnswer() {
        let question = questions[this.moveCount];
        this.sReturn.push('The right answer is:');
        this.sReturn.push(question.rightCount + '. ' + question.rightAnswer + "\n");
        this.moveCount++;
    }

    makeAMove(sInput, fCallback){
        this.sReturn = [];
        let question = questions[this.moveCount];
        let notNumber = false;
        let firstTime = false;
        if(!this.gameStart){
            this.startTheGame();
            this.sReturn.push('Welcome to \'Who wants to be a Millionaire\'');
            this.sReturn.push('Let\'s start the game: each win is $1000');
            firstTime = true
            //this.makeAQuestion();
            // fCallback(this.sReturn);
            // return;
        } else if (isNaN(sInput)) {
            this.sReturn.push('Please enter a number');
            notNumber = true
        } else if (sInput < 1 || sInput > 4) {
            this.sReturn.push('Please enter a number between 1 and 4');
            notNumber = true
        } else if (sInput == question.rightCount) {
            this.sReturn.push('Right!');
            this.makeRightAnswer();
            this.winsCount++;
        } else {
            this.sReturn.push('Wrong!');
            this.makeRightAnswer();
        }
        if (this.moveCount === questions.length) {
            this.sReturn.push('The game is ended');
            this.sReturn.push('You won $' + this.winsCount * 1000);
            this.gameStart = false;
        } else if (!notNumber) {
            if (!firstTime) this.sReturn.push('Next question');
            this.makeAQuestion();
        }
        //fCallback(this.sReturn);
        setTimeout(() => {
            fCallback(this.sReturn);
        }, 1000);
        
    }
};